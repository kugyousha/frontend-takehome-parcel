import { useState } from 'react';

function saveToLocalStorage(key, value) {
  localStorage.setItem(key, JSON.stringify(value));
}

function getFromLocalStorage(key) {
  return JSON.parse(localStorage.getItem(key));
}

export function useLocalStorage(key, defaultValue) {
  const [savedValue, setSavedValue] = useState(
    getFromLocalStorage(key) || defaultValue
  );

  function handleSave(newValue) {
    setSavedValue(newValue);
    saveToLocalStorage(key, newValue);
  }

  return [savedValue, handleSave];
}
