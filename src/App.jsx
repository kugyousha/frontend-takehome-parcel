import React from 'react';

import { GemProvider } from './contexts/gemContext';
import { GemList } from './components/GemList';
import { NavigationBar } from './components/NavigationBar';

import './App.scss';

export function App() {
  return (
    <GemProvider>
      <div className="App">
        <header className="header">Ruby Gems Searcher</header>
        <NavigationBar />
        <GemList />
      </div>
    </GemProvider>
  );
}
