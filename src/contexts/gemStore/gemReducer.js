import {
  SET_CURRENT_MODE,
  SET_SEARCHED_GEMS,
  SET_SAVED_GEMS,
} from './gemActions';

export const initialState = {
  currentMode: 'search',
  searchedGems: [],
  savedGems: {},
};

export function gemReducer(state, action) {
  switch (action.type) {
    case SET_CURRENT_MODE: {
      return {
        ...state,
        currentMode: action.payload,
      };
    }

    case SET_SEARCHED_GEMS: {
      return {
        ...state,
        searchedGems: action.payload,
      };
    }

    case SET_SAVED_GEMS: {
      return {
        ...state,
        savedGems: action.payload,
      };
    }

    default: {
      return state;
    }
  }
}
