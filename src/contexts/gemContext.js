import React, { createContext, useEffect, useReducer } from 'react';

import {
  SET_CURRENT_MODE,
  SET_SEARCHED_GEMS,
  SET_SAVED_GEMS,
} from './gemStore/gemActions';
import { useLocalStorage } from '../hooks';
import { gemReducer, initialState } from './gemStore/gemReducer';

const GemContext = createContext();

function GemProvider({ children }) {
  const [state, dispatch] = useReducer(gemReducer, initialState);
  const [savedGems, setSavedGems] = useLocalStorage('savedGems', {});

  useEffect(() => {
    dispatch({ type: SET_SAVED_GEMS, payload: savedGems });
  }, []);

  function changeMode(mode) {
    dispatch({ type: SET_CURRENT_MODE, payload: mode });
  }

  function toggleGemSaved(gem) {
    let newGems, deletedGem;
    if (savedGems[gem.name]) {
      ({ [gem.name]: deletedGem, ...newGems } = savedGems);
    } else {
      newGems = { ...savedGems, [gem.name]: gem };
    }

    setSavedGems(newGems);
    dispatch({ type: SET_SAVED_GEMS, payload: newGems });
  }

  function setSearchedGems(gems) {
    dispatch({ type: SET_SEARCHED_GEMS, payload: gems });
  }

  function isActiveMode(mode) {
    return state.currentMode === mode;
  }

  return (
    <GemContext.Provider
      value={{
        state,
        changeMode,
        toggleGemSaved,
        setSearchedGems,
        isActiveMode,
      }}>
      {children}
    </GemContext.Provider>
  );
}

export { GemContext, GemProvider };
