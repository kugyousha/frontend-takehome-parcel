import React, { useContext } from 'react';

import { useInput } from '../../hooks';
import { GemContext } from '../../contexts/gemContext';

import './NavigationBar.scss';

export function NavigationBar() {
  const { isActiveMode, changeMode, setSearchedGems } = useContext(GemContext);
  const gemQuery = useInput('');
  const GEM_API_URL = 'http://localhost:3000/api/v1/search.json?query=';

  function searchGems() {
    if (isActiveMode('saved')) {
      changeMode('search');
    }

    fetch(`${GEM_API_URL}${gemQuery.value}`)
      .then((res) => res.json())
      .then((data) => {
        setSearchedGems(data);
      })
      .catch((err) => console.error(err));
  }

  function handleClick() {
    if (isActiveMode('search')) {
      changeMode('saved');
      return;
    }

    changeMode('search');
  }

  return (
    <div className="NavigationBar">
      <div className="navigation-buttons">
        <button className="button" onClick={handleClick} data-testid="mode-button">
          {isActiveMode('search') ? 'View Saved Gems' : 'Search Gems'}
        </button>
      </div>
      <div className="search-container">
        <input className="search-input" placeholder="Search gems by name" {...gemQuery} data-testid="search-input"/>
        <button className="button" onClick={searchGems} data-testid="search-button">
          Search
        </button>
      </div>
    </div>
  );
}
