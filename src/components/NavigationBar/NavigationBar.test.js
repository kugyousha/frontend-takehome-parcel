import React from 'react';
import { NavigationBar } from './NavigationBar';
import { act, render, fireEvent, cleanup } from '@testing-library/react';

import { GemProvider, GemContext } from '../../contexts/gemContext';

afterEach(cleanup);

describe('NavigationBar', () => {
  it('should update context value of current view when button is clicked', () => {
    const modeButton = 'mode-button';
    const { getByTestId } = render(
      <GemProvider>
        <NavigationBar />
      </GemProvider>
     );

    expect(getByTestId(modeButton).textContent).toBe('View Saved Gems');
    fireEvent.click(getByTestId(modeButton));
    expect(getByTestId(modeButton).textContent).toBe('Search Gems');
  });
});



