import React, { useContext } from 'react';
import PropTypes from 'prop-types';

import { GemContext } from '../../../contexts/gemContext';

import './GemListItem.scss';

export function GemListItem({ gem }) {
  const {
    state: { savedGems },
    toggleGemSaved,
  } = useContext(GemContext);

  function handleClick() {
    toggleGemSaved(gem);
  }

  return (
    <li className="GemListItem" key={gem.name}>
      <div>
        <a className="gem-title" href={gem.homepage_uri} target="_blank">
          {gem.name}
        </a>
      </div>
      <div className="gem-info">{gem.info}</div>
      <button className="button small" onClick={handleClick} data-testid="save-button" >
        {savedGems[gem.name] ? 'unsave' : 'save'}
      </button>
    </li>
  );
}

GemListItem.propTypes = {
  gem: PropTypes.shape({
    name: PropTypes.string,
    homepage_uri: PropTypes.string,
    info: PropTypes.string,
  }).isRequired,
};
