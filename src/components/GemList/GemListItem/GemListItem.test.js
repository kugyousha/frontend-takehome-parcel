import React from 'react';
import { GemListItem } from './GemListItem';
import { render, fireEvent, cleanup } from '@testing-library/react';

import { GemProvider } from '../../../contexts/gemContext';

afterEach(cleanup);

describe('GemListItem', () => {
  it('should update context value of saved gems when button is clicked', () => {
    const saveButton = 'save-button';
    const mockGem = {
      name: 'mock-gem',
      homepage_uri: 'https://mock-gem.com',
      info: 'This is a mock gem',
    }

    const { getByTestId } = render(
      <GemProvider>
        <GemListItem gem={mockGem} />
      </GemProvider>
     );

    expect(getByTestId(saveButton).textContent).toBe('save');

    fireEvent.click(getByTestId(saveButton));

    const savedGems = JSON.parse(localStorage.getItem('savedGems'));
    expect(Object.keys(savedGems).length).toBe(1);
    expect(getByTestId(saveButton).textContent).toBe('unsave');
  });
});



