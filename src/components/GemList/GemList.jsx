import React, { useContext } from 'react';

import { GemContext } from '../../contexts/gemContext';
import { GemListItem } from './GemListItem';

import './GemList.scss';

export function GemList() {
  const {
    state: { searchedGems, savedGems },
    isActiveMode,
  } = useContext(GemContext);

  function renderGems() {
    const gems = isActiveMode('search')
      ? searchedGems
      : Object.values(savedGems);

    if (gems.length) {
      return (
        <ul className="gem-list">
          {gems.map((gem) => (
            <GemListItem key={gem.name} data-testid="list-item" gem={gem} />
          ))}
        </ul>
      );
    }

    return (
      <div className="gem-empty-list">
        {isActiveMode('search')
          ? 'No gems found in search'
          : 'No gems are currently saved'}
      </div>
    );
  }

  return <div className="GemList">{renderGems()}</div>;
}
